package com.xiaoi.south.bravefirst.dao.impl;

import com.xiaoi.south.bravefirst.entity.OrderBean;
import com.xiaoi.south.bravefirst.dao.OrderDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

/**
 * @author brave.chen
 * @create 2019-09-27 17:52
 */
@Repository("OrderDao")
public class OrderDaoImpl implements OrderDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    static class CommodityRowMapper implements RowMapper<OrderBean> {
        public  OrderBean mapRow(ResultSet rs, int nowNum) throws SQLException {
            OrderBean obj=new OrderBean();
            obj.setId(rs.getString("id"));
            obj.setUserId(rs.getString("userId"));
            obj.setCreateTime(rs.getString("createTime"));
            obj.setStat(rs.getInt("stat"));
            obj.setDesc(rs.getString("desc"));
            // 如果希望得到null，可以getObject()判断是否为null
            return obj;
        }
    }

    @Override
    public OrderBean findById(String id) {
        return null;
    }

    @Override
    public List<OrderBean> findAll() {
        return jdbcTemplate.query("select * from order",new CommodityRowMapper());
    }

    @Override
    public String save(OrderBean obj) {
        String sql="insert into order(id,userId,stat,createTime,desc)values(?,?,?,?,?)";
        obj.setId(UUID.randomUUID().toString().replaceAll("-",""));
        System.out.println(obj.toString());
        int i=jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(sql);

                ps.setString(1, obj.getId());
                ps.setString(2, obj.getUserId());
                ps.setInt(3,obj.getStat());
                ps.setString(4,obj.getCreateTime());
                ps.setString(5,obj.getDesc());
                return ps;
            }
        });
        if(i==0){
            return null;
        }
        return obj.getId();
    }

    @Override
    public void DeleteById(String id) {

    }

    @Override
    public boolean updateOrderBean(OrderBean obj) {
        return false;
    }
}
