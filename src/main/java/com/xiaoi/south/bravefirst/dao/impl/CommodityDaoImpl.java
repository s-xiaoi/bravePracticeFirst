package com.xiaoi.south.bravefirst.dao.impl;

import com.xiaoi.south.bravefirst.entity.CommodityBean;
import com.xiaoi.south.bravefirst.dao.CommodityDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.List;
import java.util.UUID;

/**
 * @author brave.chen
 * @create 2019-09-26 15:14
 */
@Repository("CommodityDao")
public class CommodityDaoImpl implements CommodityDao{

    @Autowired
    JdbcTemplate jdbcTemplate;

    static class CommodityRowMapper implements RowMapper<CommodityBean>{
        public  CommodityBean mapRow(ResultSet rs,int nowNum) throws SQLException{
            CommodityBean obj=new CommodityBean();
            obj.setId(rs.getString("id"));
            obj.setName(rs.getString("name"));
            obj.setNum(rs.getInt("num"));
            obj.setStartTime(rs.getString("startTime"));
            obj.setEndTime(rs.getString("endTime"));
            obj.setStat(rs.getInt("stat"));
            obj.setDesc(rs.getString("desc"));
            // 如果希望得到null，可以getObject()判断是否为null
            return obj;
        }
    }

    @Override
    public CommodityBean findById(String id) {
        String sql="select * from commodity where id=?";
        List<CommodityBean> list=jdbcTemplate.query(sql,new Object[]{id},
                new int[]{Types.VARCHAR},
                new CommodityRowMapper());
        if(null!=list&&!list.isEmpty()){
            return list.get(0);
        }
        return null;
    }

    @Override
    public List<CommodityBean> findAll() {

        return jdbcTemplate.query("select * from commodity",new CommodityRowMapper());
    }

    @Override
    public String save(CommodityBean obj) {
        String sql="insert into commodity(id,name,num,startTime,endTime,stat,desc)values(?,?,?,?,?,?,?)";
        obj.setId(UUID.randomUUID().toString().replaceAll("-",""));
        System.out.println(obj.toString());
        int i=jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(sql);

                ps.setString(1, obj.getId());
                ps.setString(2, obj.getName());
                ps.setInt(3,obj.getNum());
                ps.setString(4,obj.getStartTime());
                ps.setString(5,obj.getEndTime());
                ps.setInt(6,obj.getStat());
                ps.setString(7,obj.getDesc());
                return ps;
            }
        });
        System.out.println(i);
        return obj.getId();
    }

    @Override
    public void DeleteById(String id) {
        jdbcTemplate.update("delete from commodity where id=?",
                new Object[]{id},
                new int[]{Types.VARCHAR});
    }

    @Override
    public boolean updateCommodity(CommodityBean obj) {

        int i=jdbcTemplate.update(
                "update commodity set num=?,stat=? where id=?",
                new Object[]{obj.getNum(), obj.getStat(),obj.getId()});
        System.out.printf("update return i==="+i);
        if(i==0){
            return false;
        }
        return true;
    }
}
