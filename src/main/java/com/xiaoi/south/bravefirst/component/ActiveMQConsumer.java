package com.xiaoi.south.bravefirst.component;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

/**
 * @author brave.chen
 * @create 2019-10-10 13:38
 */

@Component
public class ActiveMQConsumer {


    /**
     * queue Listener
     *
     * @param text
     * @return
     */
    @JmsListener(destination = "test", containerFactory = "jmsListenerContainerQueue")
    // 将方法return回的值，再发送的"out.queue"队列中
    @SendTo("test.queue")
    public String consumeFromQueueTest(String text) {
        System.out.println("QueueListener: consumer-a 收到一条信息: " + text);
        return "consumer-a received : " + text;
    }

    @JmsListener(destination = "out.queue")
    public void consumerMsg(String msg) {
        System.out.println(msg);
    }

    @JmsListener(destination = "mqTopic", containerFactory = "jmsListenerContainerTopic")
    public void consumeFromTopic(String text) {
        System.out.println("TopicListener: consumer-a 收到一条信息: " + text);
    }


}
