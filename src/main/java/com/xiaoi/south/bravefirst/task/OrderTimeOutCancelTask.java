package com.xiaoi.south.bravefirst.task;

import com.xiaoi.south.bravefirst.entity.Producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * @author brave.chen
 * @create 2019-10-11 10:05
 */
@Component
public class OrderTimeOutCancelTask {
    private Logger LOGGER = LoggerFactory.getLogger(OrderTimeOutCancelTask.class);


    @Autowired
    private Producer producer;

    /**
     * cron表达式：Seconds Minutes Hours DayofMonth Month DayofWeek [Year] 也啊
     */
    @Scheduled(cron = "0/15 * * * * ?")
    private void cancelTimeOutOrder() {
        LOGGER.info("定时把订单写入MQ队列");

        Map<String, Object> map = new HashMap<>();

        map.put("id", 1001L);
        map.put("name", "qinxi");
        map.put("birthday", new Date());
        List<String> list = new ArrayList<>();
        list.add("aaa");
        list.add("bbb");
        list.add("ccc");
        map.put("list", list);
        producer.sendMessageQueue(map);
    }
}
