package com.xiaoi.south.bravefirst.task;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author brave.chen
 * @create 2019-10-11 10:02
 */
@Configuration
@EnableScheduling
public class SpringTaskConfig {
}
