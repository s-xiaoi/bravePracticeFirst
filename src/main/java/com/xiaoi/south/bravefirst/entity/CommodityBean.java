package com.xiaoi.south.bravefirst.entity;

import java.io.Serializable;

/**
 * @author brave.chen
 * @create 2019-09-26 15:01
 */
public class CommodityBean implements Serializable  {
    private String id;
    private String name;
    private int num;
    private String startTime;
    private String endTime;
    private int stat;
    private String desc;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public int getStat() {
        return stat;
    }

    public void setStat(int stat) {
        this.stat = stat;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "CommodityBean{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", num=" + num +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", stat=" + stat  +
                ", desc='" + desc + '\'' +
                '}';
    }
}
