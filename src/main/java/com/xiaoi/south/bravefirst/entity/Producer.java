package com.xiaoi.south.bravefirst.entity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author brave.chen
 * @create 2019-10-07 16:05
 */
@Component
public class Producer {

    @Autowired
    private JmsTemplate jmsTemplate;



    public void sendMessageTopic(String message) {
        jmsTemplate.convertAndSend("test.topic", message);
    }

    // String类型
    public void sendMessageQueue(String message) {
        jmsTemplate.convertAndSend("test.queue", message);
    }

    // 自定义对象
    public void sendMessageQueue(OrderBean data) {
        jmsTemplate.convertAndSend("test.queue.two", data);
    }

    // Map类型
    public void sendMessageQueue(Map<String, Object> map) {
        jmsTemplate.convertAndSend("test.queue.three", map);
    }


    // Map类型
    public void sendMessageQueueTemp(Map<String, Object> map) {
        //jmsTemplate.convertAndSend( map);
    }
}
