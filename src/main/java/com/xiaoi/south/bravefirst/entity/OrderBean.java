package com.xiaoi.south.bravefirst.entity;

import java.io.Serializable;

/**
 * @author brave.chen
 * @create 2019-09-26 15:05
 */
public class OrderBean implements Serializable {

    private String id;
    private String userId;
    private int stat;
    private String createTime;
    private String desc;

    @Override
    public String toString() {
        return "OrderBean{" +
                "id='" + id + '\'' +
                ", userId='" + userId + '\'' +
                ", stat=" + stat +
                ", createTime='" + createTime + '\'' +
                ", desc='" + desc + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getStat() {
        return stat;
    }

    public void setStat(int stat) {
        this.stat = stat;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
