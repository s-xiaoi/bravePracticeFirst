package com.xiaoi.south.bravefirst.entity;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author brave.chen
 * @create 2019-10-07 16:07
 */
@Component
public class Consumer {

    @JmsListener(destination = "test.queue",containerFactory = "jmsListenerContainerFactoryTopic")
    public void receiveMessageQueue(String message) {
        System.out.println("======== " + message + " =========");
    }

    /**
     *
     * @param person 自定义对象
     */
    @JmsListener(destination = "test.queue.two",containerFactory = "jmsListenerContainerFactoryTopic")
    public void receiveMessageQueue(OrderBean person) {
        System.out.println("======== " + person.toString() + " =========");
    }

    @JmsListener(destination = "test.queue.three",containerFactory = "jmsListenerContainerFactoryTopic")
    public void receiveMessageQueue(Map<String, Object> map) {
        System.out.println("=================");
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            System.out.println(entry.getKey() + " : " + entry.getValue());
        }
        System.out.println("==================");
    }

    @JmsListener(destination = "test.topic",containerFactory = "jmsListenerContainerFactoryTopic")
    public void receiveMessageTopic(String message) {
        System.out.println("========= topic first:" + message + " =========");
    }

    @JmsListener(destination = "test.topic",containerFactory = "jmsListenerContainerFactoryTopic")
    public void receiveMessageTopic2(String message) {
        System.out.println("========= topic second:" + message + " =========");
    }

}
