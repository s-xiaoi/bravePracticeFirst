package com.xiaoi.south.bravefirst.Controller;

import com.alibaba.fastjson.JSON;
import com.xiaoi.south.bravefirst.service.CommodityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author brave.chen
 * @create 2019-09-26 17:00
 */
public class GoShoppingController {

    @Autowired
    CommodityService cdtservice;


    @RequestMapping("/pickOrder")
    public String pickOrder(HttpServletRequest request,HttpServletResponse response){


        return this.writeJson(response,"111");

    }


    /**
     * 日志对象
     */
    protected Logger logger = LoggerFactory.getLogger( getClass() );

    protected String writeJsonp( HttpServletResponse response, Object obj, String callback ){
        if( obj != null ){
            String result = callback + "(" + JSON.toJSONString( obj )+ ")";
            return this.writeString( response, result, "text/json" );
        }
        return null;
    }

    /**
     * <pre>
     * <b>直接输出json(使用fastjons输出json字符串)</b>
     * </pre>
     * @param response
     * @param obj void
     * @since 1.0
     */
    protected String writeJson( HttpServletResponse response, Object obj ){
        if( obj != null ){
            String result = JSON.toJSONString( obj );
            return this.writeString( response, result, "text/json" );
        }
        return null;
    }

    /**
     * @Description <b>(直接输出字符串)</b></br>
     * @param response
     * @param str void
     * @return
     * @since 2016-9-20
     */
    protected String writeString( HttpServletResponse response, String str ){
        return this.writeString( response, str, "text/html" );
    }

    /**
     * @Description <b>(直接输出xml字符串)</b></br>
     * @param response
     * @param str void
     * @return
     * @since 2016-9-20
     */
    protected String writeXml(HttpServletResponse response, String xml ){
        return this.writeString( response, xml, "text/xml" );
    }

    /**
     * @Description <b>(直接输出字符串)</b></br>
     * @param response
     * @param str void
     * @since 2016-9-20
     */
    protected String writeString( HttpServletResponse response, String str, String type ){
        try{
            response.reset();
            response.setContentType( type );
            response.setCharacterEncoding( "utf-8" );
            // 解决跨域问题
            response.setHeader( "Access-Control-Allow-Origin", "*" );
            response.getWriter().print( str );
            return null;
        }catch( IOException e ){
            return null;
        }
    }

    /**
     * http请求参数转map
     * @param request
     * @return
     */
    @SuppressWarnings("unchecked")
    protected Map<String, Object> getParams(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<String, Object>();
        Iterator it = request.getParameterMap().entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            String key = String.valueOf(entry.getKey());
            Object[] values = (Object[]) entry.getValue();
            if (values.length <= 1) {
                map.put(key, values[0]);
            } else {
                map.put(key, values);
            }
        }
        return map;
    }

}
