package com.xiaoi.south.bravefirst.Controller;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author brave.chen
 * @create 2019-10-10 14:27
 */

@RestController
public class ConsumerThreeController
{
    /**
     * 监听队列
     * @param message
     */
    @JmsListener(destination="test.queue")
    public void readActiveTopic(String message) {
        System.out.println("接受到队列的值：" + message);
        //TODO something
    }

}
