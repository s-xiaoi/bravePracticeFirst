package com.xiaoi.south.bravefirst;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@SpringBootApplication
@EnableTransactionManagement
public class BraveFirstApplication {

	public static void main(String[] args) {
		SpringApplication.run(BraveFirstApplication.class, args);
		System.out.println("启动成功====>>>>>>>>>>>");
	}

}
