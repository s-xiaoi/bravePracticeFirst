package com.xiaoi.south.bravefirst.service.impl;

import com.xiaoi.south.bravefirst.entity.CommodityBean;
import com.xiaoi.south.bravefirst.dao.CommodityDao;
import com.xiaoi.south.bravefirst.service.CommodityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author brave.chen
 * @create 2019-09-26 15:40
 */
@Service("CommodityService")
public class CommodityServiceImpl implements CommodityService {
    @Autowired
    CommodityDao dao;
    @Override
    public CommodityBean findById(String id) {
        return dao.findById(id);
    }

    @Override
    public List<CommodityBean> findAll() {
        return dao.findAll();
    }

    @Override
    public String save(CommodityBean obj) {
        return dao.save(obj);
    }

    @Transactional
    @Override
    public void DeleteById(String id) {
        dao.DeleteById(id);
    }

    @Transactional
    @Override
    public boolean updateCommodity(CommodityBean obj) {
        return dao.updateCommodity(obj);
    }
}
