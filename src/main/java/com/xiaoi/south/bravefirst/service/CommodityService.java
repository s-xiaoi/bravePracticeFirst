package com.xiaoi.south.bravefirst.service;

import com.xiaoi.south.bravefirst.entity.CommodityBean;

import java.util.List;

/**
 * @author brave.chen
 * @create 2019-09-26 15:13
 */
public interface  CommodityService {

    CommodityBean findById(String id);
    List<CommodityBean> findAll();
    String save(CommodityBean obj);
    void DeleteById(String id);
    boolean updateCommodity(CommodityBean obj);
}
