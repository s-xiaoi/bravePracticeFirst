package com.xiaoi.south.bravefirst.service.impl;

import com.xiaoi.south.bravefirst.entity.OrderBean;
import com.xiaoi.south.bravefirst.service.OrderService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author brave.chen
 * @create 2019-09-27 17:53
 */
@Service("OrderService")
public class OrderServiceImpl implements OrderService {
    @Override
    public OrderBean findById(String id) {
        return null;
    }

    @Override
    public List<OrderBean> findAll() {
        return null;
    }

    @Override
    public String save(OrderBean obj) {
        return null;
    }

    @Override
    public void DeleteById(String id) {

    }

    @Override
    public boolean updateOrderBean(OrderBean obj) {
        return false;
    }
}
