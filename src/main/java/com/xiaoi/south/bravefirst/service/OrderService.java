package com.xiaoi.south.bravefirst.service;

import com.xiaoi.south.bravefirst.entity.OrderBean;

import java.util.List;

/**
 * @author brave.chen
 * @create 2019-09-27 17:52
 */
public interface OrderService {
    OrderBean findById(String id);
    List<OrderBean> findAll();
    String save(OrderBean obj);
    void DeleteById(String id);
    boolean updateOrderBean(OrderBean obj);
}
