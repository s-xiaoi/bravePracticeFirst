package com.xiaoi.south.bravefirst;

import com.xiaoi.south.bravefirst.entity.OrderBean;
import com.xiaoi.south.bravefirst.entity.Producer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.jms.Destination;
import java.util.*;

/**
 * @author brave.chen
 * @create 2019-10-07 16:08
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class JmsTest {

    @Autowired
    private Producer producer;

    @Test
    public void test() {
        producer.sendMessageQueue("我是queue");
        producer.sendMessageTopic("我是topic");

        OrderBean person = new OrderBean();
        person.setId("1000");
        person.setDesc("qinxi");
        person.setCreateTime((new Date()).toString());
        producer.sendMessageQueue(person);



        Map<String, Object> map = new HashMap<>();
        map.put("id", 1001L);
        map.put("name", "qinxi");
        map.put("birthday", new Date());
        List<String> list = new ArrayList<>();
        list.add("aaa");
        list.add("bbb");
        list.add("ccc");
        map.put("list", list);
        producer.sendMessageQueue(map);

        producer.sendMessageQueueTemp(map);
    }


}
