package com.xiaoi.south.bravefirst;

import com.xiaoi.south.bravefirst.entity.CommodityBean;
import com.xiaoi.south.bravefirst.service.CommodityService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BraveFirstApplicationTests {

	@Autowired
	CommodityService service;

	@Test
	public void contextLoads() {
	}

	@Test
	public void find() {
		List<CommodityBean> lists = service.findAll();
		for (CommodityBean obj : lists) {
			System.out.println(obj.toString());
		}

		CommodityBean obj = service.findById("1");
		System.out.println(obj.toString());


	}

	@Test
	public void findById() {
		CommodityBean obj = service.findById("1");
		System.out.println(obj.toString());
	}


	@Test
	public void save() {
		CommodityBean obj = new CommodityBean();
		obj.setName("tests");
		obj.setDesc("test1111");
		obj.setNum(10);
		obj.setStat(0);
		obj.setStartTime("111");
		obj.setEndTime("111");
		service.save(obj);
	}

	@Test
	public void update() {
		CommodityBean obj = new CommodityBean();
		obj.setName("苹果");
//		obj.setDesc("新增商品");
		obj.setNum(10);
		obj.setStat(2);
		obj.setId("1");
		System.out.println(service.updateCommodity(obj));
	}
}